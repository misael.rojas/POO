<?php namespace Models;

class Estudiante{

    private $id;
    private $nombre;
    private $edad;
    private $promedio;
    private $imagen;
    private $id_seccion;
    private $fecha;
    private $con;

    public function __construct(){
        $this->con = new Conexion();
    }

    public function set($atributo, $contenido){
        $this->$atributo=$contenido;
    }

    public function get($atributo){
        return $this->$atributo;
    }

    public function listar(){
        $sql = "SELECT Es.*, Se.nombre as nombre_seccion FROM estudiantes Es INNER JOIN secciones Se ON Es.id_seccion=Se.id";
        $this->con->consultaRetorno($sql);
    }

    public function add(){
        $sql="INSERT INTO estudiantes(nombre, edad, promedio, imagen, id_seccion, fecha)
              VALUES ('{$this->nombre}','{$this->edad}','{$this->promedio}','{$this->imagen}','{$this->id_seccion}',now())";

        $this->con->consultaSimple($sql);
    }

    public function delete(){
        $sql="DELETE FROM estudiantes WHERE id = '{$this->id}'";
        $this->con->consultaSimple($sql);
    }

    public function edit(){
        $sql="UPDATE estudiantes SET
            nombre='{$this->nombre}',
            edad='{$this->edad}',
            promedio='{$this->promedio}',
            imagen='{$this->imagen}',
            id_seccion='{$this->id_seccion}',
            WHERE id = '{$this->id}'";

        $this->con->consultaSimple($sql);
    }

    public function view(){
        $sql = "SELECT Es.*, Se.nombre as nombre_seccion FROM estudiantes Es INNER JOIN secciones Se ON Es.id_seccion=Se.id WHERE Es.id='{$this->id}'";
        $row=$this->con->consultaRetorno($sql);
        $datos=$row->fetch_assoc();
        return $datos;

    }
}

